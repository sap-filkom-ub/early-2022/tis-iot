// Arduino package
#include <Arduino.h>

// Wifi package
#if defined(ESP32)
#include <WiFi.h>
#elif defined(ESP8266)
#include <ESP8266WiFi.h>
#endif
#include <Firebase_ESP_Client.h>

// Timestamp package
#include <NTPClient.h>
#include <WiFiUdp.h>

// Sensor package
#include <DHT.h>

// Provide the token generation process info.
#include "addons/TokenHelper.h"
// Provide the RTDB payload printing info and other helper functions.
#include "addons/RTDBHelper.h"

// Insert your network credentials
#define WIFI_SSID "ANTIK"
#define WIFI_PASSWORD "antik2109"

// Insert Firebase project API Key
#define API_KEY "AIzaSyDgwEX38RTNGJVTaozetuAspm5MCcsZ1l4"

// Insert RTDB URL define the RTDB URL
#define DATABASE_URL "https://iot-monitoring-5d1eb-default-rtdb.firebaseio.com/"

// Insert Node Config
#define USER_EMAIL "syahrul@email.com"
#define USER_PASSWORD "password"
#define NODE_ID "-N2qklyQUXVEWk_wmd8h"

/* Defining the pin and type of the sensor. */
#define DHTPIN D7
#define DHTTYPE DHT11
DHT dht(DHTPIN, DHTTYPE);

// Define Firebase Data object
FirebaseData fbdo;
FirebaseAuth auth;
FirebaseConfig config;
String uid;
String databasePath;

unsigned long sendDataPrevMillis = 0;
bool signupOK = false;

// Define NTP Client to get time
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "pool.ntp.org");
int timestamp;

// LED stuff
#define LED 2

void setup()
{
    // put your setup code here, to run once:
    Serial.begin(115200);
    WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
    Serial.print("Connecting to Wi-Fi");
    while (WiFi.status() != WL_CONNECTED)
    {
        Serial.print(".");
        delay(300);
    }
    Serial.println();
    Serial.print("Connected with IP: ");
    Serial.println(WiFi.localIP());

    /* Assign the api key (required) */
    config.api_key = API_KEY;

    /* Assign the RTDB URL (required) */
    config.database_url = DATABASE_URL;

    // Assign the user sign in credentials
    auth.user.email = USER_EMAIL;
    auth.user.password = USER_PASSWORD;

    // /* Sign up for anonymus auth */
    // if (Firebase.signUp(&config, &auth, "", ""))
    // {
    //     Serial.println("ok");
    //     Serial.println();
    //     signupOK = true;
    // }
    // else
    // {
    //     Serial.printf("%s\n", config.signer.signupError.message.c_str());
    // }

    /* Assign the callback function for the long running token generation task */
    config.token_status_callback = tokenStatusCallback; // see addons/TokenHelper.h
    // Assign the maximum retry of token generation
    config.max_token_generation_retry = 5;

    Firebase.begin(&config, &auth);
    Firebase.reconnectWiFi(true);

    // Getting the user UID might take a few seconds
    Serial.println("Getting User UID");
    while ((auth.token.uid) == "")
    {
        Serial.print('.');
        delay(1000);
    }
    uid = auth.token.uid.c_str();
    Serial.print("User UID: ");
    Serial.println(uid);
    Serial.println();

    timeClient.begin();
    // Set offset time in seconds to adjust for your timezone, for example:
    // GMT +1 = 3600
    // GMT +8 = 28800
    // GMT -1 = -3600
    // GMT 0 = 0
    timeClient.setTimeOffset(25200);

    // LED Stuff
    pinMode(LED, OUTPUT);
}

void loop()
{
    // put your main code here, to run repeatedly:
    // Blinking the LED.
    digitalWrite(LED, LOW);
    delay(500);
    digitalWrite(LED, HIGH);

    // if (Firebase.ready() && signupOK && (millis() - sendDataPrevMillis > 15000 || sendDataPrevMillis == 0))
    if (Firebase.ready() && (millis() - sendDataPrevMillis > 15000 || sendDataPrevMillis == 0))
    {
        sendDataPrevMillis = millis();

        /* Getting the current time from the NTP server. */
        timeClient.update();
        time_t epochTime = timeClient.getEpochTime();
        String formattedTime = timeClient.getFormattedTime();
        // Get a time structure
        struct tm *ptm = gmtime((time_t *)&epochTime);
        int monthDay = ptm->tm_mday;
        int currentMonth = ptm->tm_mon + 1;
        int currentYear = ptm->tm_year + 1900;
        String currentDate = String(currentYear) + "-" + String(currentMonth) + "-" + String(monthDay) + " " + formattedTime;

        // String uid = USER_ID;
        String nid = NODE_ID;
        String path = "users/" + uid + "/nodes/" + nid + "/data/" + currentDate;

        /* Reading the humidity and temperature from the sensor. */
        float hum = dht.readHumidity();
        delay(10);
        float tem = dht.readTemperature();

        // Write an data on the database path /humidity
        if (Firebase.RTDB.setInt(&fbdo, path + "/humidity", hum))
        {
            Serial.println("PASSED");
            Serial.println("PATH: " + fbdo.dataPath() + "/" + hum);
        }
        else
        {
            Serial.println("FAILED");
            Serial.println("REASON: " + fbdo.errorReason() + "/" + hum);
        }

        // Write an data on the database path /temperature
        if (Firebase.RTDB.setInt(&fbdo, path + "/temperature", tem))
        {
            Serial.println("PASSED");
            Serial.println("PATH: " + fbdo.dataPath() + "/" + tem);
        }
        else
        {
            Serial.println("FAILED");
            Serial.println("REASON: " + fbdo.errorReason() + "/" + tem);
        }

        delay(30000);
    }
}
